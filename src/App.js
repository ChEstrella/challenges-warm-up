import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from "./components/Header/index";
import Home from "./components/Home/index";
import DescriptionPost from './components/DescriptionPost/index'

export default function App() {
            
    return (
      <Router>
        <Header />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route  path="/descriptionPost/:id">
            <DescriptionPost />
          </Route>
        </Switch>
      
      </Router>
    );
}
