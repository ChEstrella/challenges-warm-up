import React from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import { useHistory } from "react-router-dom";
import "../../styles/Header.css";

export default function Header() {
  const history = useHistory();
  return (
    <div className="Navbar">
      <AppBar className="Navbar__Container">
        <Toolbar className="Navbar__Toolbar">
          <IconButton
            edge="start"
            color="inherit"
            aria-label="menu"
          ></IconButton>
          <Typography
            className="Navbar__Title"
            onClick={() => history.push("/")}
            variant="h6"
          >
            Challenge
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}
