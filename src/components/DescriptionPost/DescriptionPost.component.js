import React from "react";
import CardPost from "../CardPost/index";
import "../../styles/DescriptionPost.css";

const DescriptionPostComponent = ({ post }) => {
  return (
    <div className="PostDescription">
      <CardPost post={post} />
    </div>
  );
};

export default DescriptionPostComponent;
