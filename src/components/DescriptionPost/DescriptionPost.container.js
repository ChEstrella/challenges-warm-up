import React, { useEffect, useState } from "react";
import axios from "axios";
import DescriptionPostComponent from "./DescriptionPost.component";

const DescriptionPostContainer = () => {
  const [post, setPost] = useState([]);
  const url = window.location.pathname;
  const idPost = url.substring(url.lastIndexOf("/") + 1);

  useEffect(() => {
    axios
      .get(`https://jsonplaceholder.typicode.com/posts/${idPost}`)
      .then((res) => {
        setPost(res.data);
      });
  }, []);

  return <DescriptionPostComponent post={post} />;
};

export default DescriptionPostContainer;
