import React, { useEffect, useState } from "react";
import axios from "axios";
import EditPostComponent from "./EditPost.component";

const EditPost = ({ post, openModal, alertMessage, open }) => {
  return (
    <EditPostComponent
      post={post}
      openModal={openModal}
      alertMessage={alertMessage}
      open={open}
    />
  );
};

export default EditPost;
