import React, { useState } from "react";
import {
  FormControl,
  TextField,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from "@material-ui/core";
import axios from "axios";
import "../../styles/EditPost.css";

const EditPostComponent = ({ post, openModal, open, alertMessage }) => {
  const [errorMessage, setErrorMessage] = useState("");
  const [formData, setFormData] = useState(post);

  const onPropertyChange = (property) => ({ target: { value } }) => {
    let newFormData = { ...formData };
    newFormData[property] = value;
    setFormData(newFormData);
  };

  const isValidate = () => {
    let errorMessage = "";
    if (formData.title === "" || formData.body === "") {
      errorMessage = "**Los datos son requeridos";
      setErrorMessage(errorMessage);
    }
    return !(errorMessage != "");
  };

  const handleSave = async () => {
    if (!!post.title && isValidate()) {
      axios
        .put(
          `https://jsonplaceholder.typicode.com/posts/${formData.id}`,
          formData
        )
        .then((response) => {
          handleClickOpen();
          alertMessage({
            type: "success",
            message: "Su post se guardo con exito",
          });
        })
        .catch((error) => {
          handleClickOpen();
          alertMessage({
            type: "error",
            message: "Su post no se pudo guardar",
          });
        });
    }
    if (!post.title && isValidate()) {
      axios
        .post(`https://jsonplaceholder.typicode.com/posts`, formData)
        .then((response) => {
          handleClickOpen();
          alertMessage({
            type: "success",
            message: "Su post se guardo con exito",
          });
        })
        .catch((error) => {
          alertMessage({
            type: "error",
            message: "Su post no se pudo guardar",
          });
        });
    }
  };

  const handleClose = () => {
    openModal(false);
  };

  const handleClickOpen = () => {
    openModal(false);
  };
  return (
    <>
      <Dialog
        open={open}
        maxWidth="md"
        fullWidth
        aria-labelledby="form-dialog-title"
        className="Dialog"
      >
        <DialogTitle id="form-dialog-title">Post</DialogTitle>
        <DialogContent className="Dialog__Content">
          <FormControl>
            <TextField
              label="*Title"
              multiline={true}
              value={formData.title != undefined ? formData.title : ""}
              onChange={onPropertyChange("title")}
              margin="normal"
            />
          </FormControl>
          <FormControl>
            <TextField
              label="*Boody"
              fullWidth
              multiline={true}
              value={formData.body != undefined ? formData.body : ""}
              onChange={onPropertyChange("body")}
              margin="normal"
            />
          </FormControl>
          {errorMessage != "" && (
            <label className="Dialog__errorMessage">{errorMessage}</label>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            cerrar
          </Button>
          <Button onClick={handleSave} color="primary">
            Guardar
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default EditPostComponent;
