import React, { useState } from "react";
import Alert from "@material-ui/lab/Alert";
import { Button } from "@material-ui/core/";
import EditPost from "../EditPost/index";
import CardPost from "../../components/CardPost/index";
import "../../styles/Home.css";

const HomeComponent = ({ posts }) => {
  const [alertMessage, setAlertMessage] = useState({});
  const [openModal, setOpenModal] = useState(false);
  const [editPost, setEditPost] = useState({});
  const [descriptionPost, setDescriptionPost] = useState({});

  const handleNewPost = () => {
    setOpenModal(true);
    setEditPost({ title: "", body: "" });
  };

  return (
    <div className="Home">
      <Button
        variant="contained"
        className="Home__Button"
        onClick={handleNewPost}
        color="primary"
      >
        New Post
      </Button>
      {!!alertMessage.type && (
        <Alert className="Home__Alert" severity={alertMessage.type}>
          {alertMessage.message}
        </Alert>
      )}
      <div className="Home__Post">
        {!!posts &&
          posts.length > 0 &&
          posts.map((post) => (
            <CardPost
              editPost={setEditPost}
              openModal={setOpenModal}
              descriptionPost={setDescriptionPost}
              key={post.id}
              post={post}
              alertMessage={setAlertMessage}
            />
          ))}
      </div>

      {!!openModal && (
        <EditPost
          open={openModal}
          alertMessage={setAlertMessage}
          post={editPost}
          openModal={setOpenModal}
        />
      )}
    </div>
  );
};

export default HomeComponent;
