import React, { useEffect, useState } from "react";
import axios from "axios";
import HomeComponent from "./Home.component";

const Home = () => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    axios.get(`https://jsonplaceholder.typicode.com/posts`).then((res) => {
      setPosts(res.data);
    });
  }, []);

  return <HomeComponent posts={posts} />;
};

export default Home;
