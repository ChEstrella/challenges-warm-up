import React from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { Card, CardContent, CardActions, Typography } from "@material-ui/core/";
import { ZoomIn, Edit, DeleteForever } from "@material-ui/icons";
import "../../styles/CardPost.css";

const CardPost = ({ post, editPost, openModal, alertMessage }) => {
  const history = useHistory();

  const handleIdPost = () => {
    history.push(`/descriptionPost/${post.id}`);
  };

  const onClickOpenModal = () => {
    editPost(post);
    openModal(true);
  };

  const handleDeletePost = () => {
    axios
      .delete(`https://jsonplaceholder.typicode.com/posts/${post.id}`)
      .then((res) => {
        alertMessage({
          type: "success",
          message: "Su post se elimino con exito",
        });
      });
  };

  return (
    <Card className="CardPost">
      <CardContent className="CardPost__Content">
        <Typography
          className="CardPost__Content__Title"
          variant="h5"
          component="h2"
        >
          {post.title}
        </Typography>
        <Typography
          className="CardPost__Content__Subtitle"
          color="textSecondary"
          gutterBottom
        >
          {post.body}
        </Typography>
      </CardContent>
      <CardActions className="CardPost__Actions">
        <ZoomIn className="CardPost__Icon" onClick={handleIdPost} />
        <Edit className="CardPost__Icon" onClick={onClickOpenModal} />
        <DeleteForever onClick={handleDeletePost} className="CardPost__Icon" />
      </CardActions>
    </Card>
  );
};

export default CardPost;
